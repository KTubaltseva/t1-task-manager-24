package ru.t1.ktubaltseva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.enumerated.Sort;
import ru.t1.ktubaltseva.tm.enumerated.Status;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.auth.AuthRequiredException;
import ru.t1.ktubaltseva.tm.exception.field.AbstractFieldException;
import ru.t1.ktubaltseva.tm.exception.field.NameEmptyException;
import ru.t1.ktubaltseva.tm.model.Project;

import java.util.List;

public interface IProjectService extends IUserOwnedService<Project> {

    @NotNull
    Project create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    ) throws AbstractFieldException, AuthRequiredException;

    @NotNull
    Project create(
            @Nullable String userId,
            @Nullable String name
    ) throws NameEmptyException, AuthRequiredException;

    @NotNull
    Project changeProjectStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    ) throws AbstractException;

    @NotNull
    Project changeProjectStatusByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable Status status
    ) throws AbstractException;

    @NotNull
    List<Project> findAll(
            @Nullable String userId,
            @Nullable Sort sort
    ) throws AuthRequiredException;

    @NotNull
    Project updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws AbstractException;

    @NotNull
    Project updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    ) throws AbstractException;

}
