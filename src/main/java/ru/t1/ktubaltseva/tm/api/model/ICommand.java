package ru.t1.ktubaltseva.tm.api.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.enumerated.Role;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

import java.security.NoSuchAlgorithmException;

public interface ICommand {

    @NotNull
    String getName();

    @Nullable
    String getArgument();

    @NotNull
    String getDescription();

    @Nullable
    Role[] getRoles();

    void execute() throws AbstractException, NoSuchAlgorithmException;

    @NotNull
    @Override
    String toString();

}
