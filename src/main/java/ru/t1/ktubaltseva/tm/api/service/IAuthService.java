package ru.t1.ktubaltseva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.enumerated.Role;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.auth.AbstractAuthException;
import ru.t1.ktubaltseva.tm.exception.auth.AuthRequiredException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.UserNotFoundException;
import ru.t1.ktubaltseva.tm.model.User;

import java.security.NoSuchAlgorithmException;

public interface IAuthService {

    void checkRoles(@Nullable Role[] roles) throws UserNotFoundException, AbstractAuthException;

    @NotNull
    String getUserId() throws AuthRequiredException;

    @NotNull
    User getUser() throws UserNotFoundException, AuthRequiredException;

    boolean isAuth();

    void login(
            @Nullable String login,
            @Nullable String password
    ) throws AbstractException, NoSuchAlgorithmException;

    void logout();

    @NotNull
    User registry(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    ) throws AbstractException, NoSuchAlgorithmException;

}
