package ru.t1.ktubaltseva.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.component.Bootstrap;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

import java.security.NoSuchAlgorithmException;

public class Application {

    public static void main(@Nullable final String[] args) throws AbstractException, NoSuchAlgorithmException {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}