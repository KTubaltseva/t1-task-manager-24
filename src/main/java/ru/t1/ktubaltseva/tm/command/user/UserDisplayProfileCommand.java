package ru.t1.ktubaltseva.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.enumerated.Role;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

public class UserDisplayProfileCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "user-display-profile";

    @NotNull
    private final String DESC = "Display user profile.";

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DISPLAY USER PROFILE]");
        displayUser(getAuthService().getUser());
    }

}
