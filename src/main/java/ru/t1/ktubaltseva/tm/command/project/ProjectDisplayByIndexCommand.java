package ru.t1.ktubaltseva.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.util.TerminalUtil;

public final class ProjectDisplayByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private final String NAME = "project-display-by-index";

    @NotNull
    private final String DESC = "Display project by index.";


    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DISPLAY PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextInt() - 1;
        @NotNull final Project project = getProjectService().findOneByIndex(getUserId(), index);
        displayProject(project);
    }

}
